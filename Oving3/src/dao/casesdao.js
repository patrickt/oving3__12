// @flow
/* eslint eqeqeq: "off" */

const Dao = require("./dao.js");

module.exports = class CasesDao extends Dao {
  getAll(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where importance=1",
      [],
      callback
    );
  }

  getAll2(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase",
      [],
      callback
    );
  }

  getOne(id, callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where id=?",
      [id],
      callback
    );
  }

  getNews(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where category='Nyheter'",
      [],
      callback
    );
  }

  getSports(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where category='Sport'",
      [],
      callback
    );
  }

  getPolitics(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where category='Politikk'",
      [],
      callback
    );
  }

  getCulture(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where category='Kultur'",
      [],
      callback
    );
  }

  getGames(callback) {
    super.query(
      "select id, title, titleText, content, date, picture, category, importance from newsCase where category='Dataspill'",
      [],
      callback
    );
  }

  getLatest(callback) {
    super.query(
      "SELECT * FROM newsCase ORDER BY id DESC LIMIT 4;",
      [],
      callback
    );
  }

  createOne(json, callback) {
    var val = [
      json.title,
      json.titleText,
      json.content,
      json.date,
      json.picture,
      json.category,
      json.importance
    ];
    super.query(
      "insert into newsCase (title,titleText,content,date,picture,category,importance) values (?,?,?,?,?,?,?)",
      val,
      callback
    );
  }

  updateOne(json, id, callback) {
    var val = [json.title, json.titleText, json.content, json.picture, id];
    super.query(
      "UPDATE newsCase SET title=?, titleText=?, content=?, picture=? WHERE id=?",
      val,
      callback
    );
  }

  deleteOne(id, callback) {
    super.query("delete from newsCase where id=?", [id], callback);
  }
};
