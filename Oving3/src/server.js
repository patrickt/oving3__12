// @flow

var express = require("express");
var mysql = require("mysql");
var bodyParser = require("body-parser");
var app = express();
var apiRoutes = express.Router();
app.use(bodyParser.json()); // for å tolke JSON
const CasesDao = require("./dao/casesdao.js");
var cors = require("cors");
app.use(cors());

var pool = mysql.createPool({
  connectionLimit: 2,
  host: "mysql.stud.iie.ntnu.no",
  user: "patrickt",
  password: "BTdk4yNw",
  database: "patrickt",
  debug: false
});

let casesDao = new CasesDao(pool);

//Get all newscases with importance 1
app.get("/cases", (req, res) => {
  console.log("/cases: fikk request fra klient!");
  casesDao.getAll((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all newscases
app.get("/casesall", (req, res) => {
  console.log("/cases: fikk request fra klient!");
  casesDao.getAll2((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get a specific newscase
app.get("/casesall/:id", (req, res) => {
  console.log("/casesall/:id: fikk request fra klient!");
  casesDao.getOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all politics cases
app.get("/politicslist", (req, res) => {
  console.log("/politcslist: fikk request fra klient!");
  casesDao.getPolitics((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all News cases
app.get("/newslist", (req, res) => {
  console.log("/newslist: fikk request fra klient!");
  casesDao.getNews((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all Sports cases
app.get("/sportlist", (req, res) => {
  console.log("/sportlist: fikk request fra klient!");
  casesDao.getSports((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all Games cases
app.get("/gameslist", (req, res) => {
  console.log("/gameslist: fikk request fra klient!");
  casesDao.getGames((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get all Culture cases
app.get("/culturelist", (req, res) => {
  console.log("/newslist: fikk request fra klient!");
  casesDao.getCulture((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Add a new case
app.post("/cases", (req, res) => {
  console.log("Fikk POST-request fra klienten!");
  casesDao.createOne(req.body, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Delete a case
app.delete("/cases/:id", (req, res) => {
  console.log("/cases/:id: fikk DELETE request fra klient!");
  casesDao.deleteOne(req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Get 4 latest cases
app.get("/caseslatest", (req, res) => {
  console.log("/cases: Getting 4 latest cases");
  casesDao.getLatest((status, data) => {
    res.status(status);
    res.json(data);
  });
});

//Update a case
app.put("/casesall/:id", (req, res) => {
  console.log("/casesall/:id: fikk PUT request fra klient!");
  casesDao.updateOne(req.body, req.params.id, (status, data) => {
    res.status(status);
    res.json(data);
  });
});

var server = app.listen(8080);
