// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from "react-dom";
import * as React from "react";
import { Component } from "react-simplified";
import { HashRouter, Route, NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import createHashHistory from "history/createHashHistory";
const history = createHashHistory();

class FindDate {
  day: number;
  month;

  constructor() {
    var today = new Date();
    this.day = today.getDate();
    this.month = today.getMonth() + 1;
    if (this.month == 1) {
      this.month = "Jan";
    } else if (this.month == 10) {
      this.month = "Okt";
    } else if (this.month == 2) {
      this.month = "Feb";
    } else if (this.month == 3) {
      this.month = "Mar";
    } else if (this.month == 4) {
      this.month = "Apr";
    } else if (this.month == 5) {
      this.month = "May";
    } else if (this.month == 6) {
      this.month = "Jun";
    } else if (this.month == 7) {
      this.month = "Jul";
    } else if (this.month == 8) {
      this.month = "Aug";
    } else if (this.month == 9) {
      this.month = "Sep";
    } else if (this.month == 11) {
      this.month = "Nov";
    } else {
      this.month = "Des";
    }
  }
}

class NewsCase {
  id: number;
  static nextId = 1;

  title: string;
  date: string;
  category: string;
  pictureUrl: string;
  text: string;

  constructor(
    title: string,
    date: string,
    category: string,
    pictureUrl: string,
    text: string
  ) {
    this.title = title;
    this.date = date;
    this.category = category;
    this.pictureUrl = pictureUrl;
    this.text = text;
  }
}

class LatestCases extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/caseslatest", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    var text = "";
    return (
      <div
        class="card"
        style={{ width: "1750px", margin: "auto", top: "10px" }}
      >
        <div class="card-body">
          <marquee>
            {items.map(NewsCase => (
              <b style={{ margin: "100px" }}>
                {NewsCase.title} - {NewsCase.date}
              </b>
            ))}
          </marquee>
        </div>
      </div>
    );
  }
}

class NewsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/cases", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong
                      class="d-inline-block mb-2 text-success"
                      style={{ color: "white" }}
                    >
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#">
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <NavLink exact to={"/" + NewsCase.id}>
                      Les videre
                    </NavLink>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class OnlyNewsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/newslist", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    console.log(items);
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#">
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <NavLink exact to={"/nyheter/" + NewsCase.id}>
                      Les videre
                    </NavLink>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class SportList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/sportlist", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#" style={{ color: "white" }}>
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <NavLink exact to={"/sport/" + NewsCase.id}>
                      Les videre
                    </NavLink>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class CultureList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/culturelist", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#" style={{ color: "white" }}>
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <NavLink exact to={"/kultur/" + NewsCase.id}>
                      Les videre
                    </NavLink>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class GamesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/gameslist", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#" style={{ color: "white" }}>
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <NavLink exact to={"/dataspill/" + NewsCase.id}>
                      Les videre
                    </NavLink>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class PoliticsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/politicslist", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div style={{ background: "#24282c" }}>
          <div class="row mb-2" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="col-md-6" key={NewsCase.id}>
                <div
                  class="card flex-md-row mb-4 shadow-sm h-md-250"
                  style={{ background: "#1f2226" }}
                >
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">
                      {NewsCase.category}
                    </strong>
                    <h3 class="mb-0">
                      <a class="text-light" href="#" style={{ color: "white" }}>
                        {NewsCase.title}
                      </a>
                    </h3>
                    <div class="mb-1 text-muted">{NewsCase.date}</div>
                    <p class="card-text mb-auto" style={{ color: "white" }}>
                      {NewsCase.titleText}
                    </p>
                    <a href="#">Les videre</a>
                  </div>
                  <img
                    class="card-img-right flex-auto d-none d-lg-block"
                    src={NewsCase.picture}
                    alt="Card image cap"
                    height="300px"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

class DeleteAndEdit extends Component<{
  match: { params: { id: number } }
}> {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    console.log(this.props.match.params.id);
    fetch("http://localhost:8080/casesall", {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div
          class="mx-auto"
          style={{ margin: "50px", height: "3000px", width: "1000px" }}
        >
          <li
            class="list-group list-group-item-action"
            style={{ width: "1000px" }}
            key={NewsCase.id}
          >
            {items.map(NewsCase => (
              <li
                class="list-group-item list-group-item-action"
                key={NewsCase.id}
              >
                {NewsCase.title} - {NewsCase.date}
                <NavLink
                  exact
                  to={"/edit/" + NewsCase.id}
                  style={{ float: "right" }}
                  class="btn btn-primary"
                >
                  Edit
                </NavLink>
                <button
                  type="delete"
                  class="btn btn-danger"
                  onClick={this.delete.bind(null, NewsCase.id)}
                  style={{ margin: "10px", float: "right" }}
                >
                  Delete
                </button>
              </li>
            ))}
          </li>
        </div>
      );
    }
  }

  delete(id: number) {
    const { error, isLoaded, items } = this.state;
    console.log(id);
    var url = "http://localhost:8080/cases/" + id;
    console.log(url);
    fetch(url, {
      method: "DELETE"
    }).then(res => res.json());
    this.setState({ state: this.state });
  }
}

class CaseEdit extends Component<{
  match: { params: { id: number } }
}> {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  title = "";
  titleText = "";
  content = "";
  picture = "";

  componentDidMount() {
    var url = "http://localhost:8080/casesall/" + this.props.match.params.id;
    fetch(url, {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    console.log(this.title);
    window.scrollTo(0, 0);
    return (
      <div
        class="mx-auto"
        style={{ margin: "100px", height: "3000px", width: "1000px" }}
      >
        {items.map(NewsCase => (
          <div>
            <form>
              <div class="form-group">
                <label for="inpTitle" style={{ color: "white" }}>
                  Tittel
                </label>
                <input
                  type="title"
                  defaultValue={NewsCase.title}
                  onChange={event => (this.title = event.target.value)}
                  class="form-control"
                  id="inpTitle"
                  aria-describedby="titleHelp"
                />
                <small id="titleHelp" class="form-text text-muted">
                  Tittlen bør vekke interesse for innlegget ditt!
                </small>
              </div>
              <div class="form-group">
                <label for="inpText" style={{ color: "white" }}>
                  Forsidetekst
                </label>
                <input
                  type="text"
                  defaultValue={NewsCase.titleText}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.titleText = event.target.value)
                  }
                  class="form-control"
                  id="inptitleText"
                />
                <small id="textHelp" class="form-text text-muted">
                  Denne teksten vil vises på forsiden sammen med tittlen og
                  bildet ditt
                </small>
              </div>
              <div class="form-group">
                <label for="inpInnleggstekst" style={{ color: "white" }}>
                  Innleggstekst
                </label>
                <textarea
                  type="text"
                  defaultValue={NewsCase.content}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.content = event.target.value)
                  }
                  class="form-control"
                  id="inpContent"
                  rows="5"
                />
              </div>
              <div class="form-group">
                <label for="inpUrl" style={{ color: "white" }}>
                  Url til bilde
                </label>
                <input
                  type="text"
                  defaultValue={NewsCase.picture}
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.picture = event.target.value)
                  }
                  class="form-control"
                  id="inpUrl"
                />
              </div>
              <button type="submit" class="btn btn-primary" onClick={this.edit}>
                Edit
              </button>
            </form>
          </div>
        ))}
      </div>
    );
  }

  edit() {
    let url = "http://localhost:8080/casesall/" + this.props.match.params.id;
    console.log(url);
    fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        title: this.title,
        titleText: this.titleText,
        content: this.content,
        picture: this.picture,
        id: this.props.match.params.id
      })
    });
    history.push("/nysak");
  }
}

class NewCase extends Component {
  title = "";
  titleText = "";
  date = "";
  category = "Nyheter";
  pictureUrl = "";
  content = "";
  importance = "1";

  render() {
    return (
      <div class="mx-auto" style={{ width: "1000px", margin: "20px" }}>
        <form>
          <div class="form-group">
            <label for="inpTitle" style={{ color: "white" }}>
              Tittel
            </label>
            <input
              type="title"
              value={this.title}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.title = event.target.value)
              }
              class="form-control"
              id="inpTitle"
              aria-describedby="titleHelp"
              placeholder="Tittel på innlegget"
            />
            <small id="titleHelp" class="form-text text-muted">
              Tittlen bør vekke interesse for innlegget ditt!
            </small>
          </div>
          <div class="form-group">
            <label for="inpText" style={{ color: "white" }}>
              Forsidetekst
            </label>
            <input
              type="text"
              value={this.titleText}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.titleText = event.target.value)
              }
              class="form-control"
              id="inptitleText"
              placeholder="Forsidetekst"
            />
            <small id="textHelp" class="form-text text-muted">
              Denne teksten vil vises på forsiden sammen med tittlen og bildet
              ditt
            </small>
          </div>
          <div class="form-group">
            <label for="inpCategory" style={{ color: "white" }}>
              Kategori som passer til innlegget ditt
            </label>
            <select
              class="form-control"
              id="inpCategory"
              value={this.category}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.category = event.target.value)
              }
            >
              <option>Nyheter</option>
              <option>Sport</option>
              <option>Kultur</option>
              <option>Dataspill</option>
              <option>Politikk</option>
            </select>
          </div>
          <div class="form-group">
            <label for="inpInnleggstekst" style={{ color: "white" }}>
              Innleggstekst
            </label>
            <textarea
              type="text"
              value={this.content}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.content = event.target.value)
              }
              class="form-control"
              id="inpContent"
              rows="5"
            />
          </div>
          <fieldset class="form-group">
            <legend style={{ color: "white" }}>Viktighet</legend>
            <div class="form-check">
              <label class="form-check-label" style={{ color: "white" }}>
                <input
                  type="radio"
                  class="form-check-input"
                  name="optionsRadios"
                  id="optionsRadios1"
                  value="1"
                  checked
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.importance = event.target.value)
                  }
                />
                Veldig viktig - hvis du velger dette vil saken din vises på
                forsiden
              </label>
            </div>
            <div class="form-check">
              <label class="form-check-label" style={{ color: "white" }}>
                <input
                  type="radio"
                  class="form-check-input"
                  name="optionsRadios"
                  id="optionsRadios2"
                  value="2"
                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                    (this.importance = event.target.value)
                  }
                />
                Mindre viktig - hvis du velger dette vil saken din kun vises
                under kategorien du har valgt
              </label>
            </div>
          </fieldset>
          <div class="form-group">
            <label for="inpUrl" style={{ color: "white" }}>
              Url til bilde
            </label>
            <input
              type="text"
              value={this.pictureUrl}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.pictureUrl = event.target.value)
              }
              class="form-control"
              id="inpUrl"
              placeholder="https://"
            />
          </div>
          <button type="submit" class="btn btn-primary" onClick={this.save}>
            Post
          </button>
        </form>
      </div>
    );
  }

  save() {
    console.log(this.title);
    console.log(this.importance);
    console.log(this.category);
    var day = new FindDate();
    fetch("http://localhost:8080/cases", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        title: this.title,
        titleText: this.titleText,
        content: this.content,
        date: day.month + " " + day.day,
        picture: this.pictureUrl,
        category: this.category,
        importance: this.importance
      })
    });
    history.push("/");
  }
}

class Menu extends Component {
  render() {
    return (
      <nav
        class="navbar navbar-expand-lg navbar-dark bg-dark"
        style={{ background: "#1f2226" }}
      >
        <NavLink class="navbar-brand" exact to="/">
          <img
            src="https://fanart.tv/fanart/movies/8388/hdmovielogo/three-amigos-5871a7ecacb18.png"
            height="74"
            width="200"
            alt="Gå til forsiden"
          />
        </NavLink>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/nyheter"
                style={{ margin: "20px" }}
              >
                Nyheter
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/sport"
                style={{ margin: "20px" }}
              >
                Sport
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/dataspill"
                style={{ margin: "20px" }}
              >
                Dataspill
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/politikk"
                style={{ margin: "20px" }}
              >
                Politikk
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/kultur"
                style={{ margin: "20px" }}
              >
                Kultur
              </NavLink>
            </li>
          </ul>
        </div>
        <div
          class="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul class="navbar-nav">
            <li class="nav-item">
              <NavLink class="nav-link" exact to="/nysak">
                Skriv ny sak
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

class CaseDetails extends Component<{
  match: { params: { id: number } }
}> {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    var url = "http://localhost:8080/casesall/" + this.props.match.params.id;
    console.log(url);
    fetch(url, {
      method: "GET"
    })
      .then(res => res.json())
      //.then(json => console.log(JSON.stringify(json)))
      .then(
        res => {
          console.log(res);
          this.setState({
            isLoaded: true,
            items: res
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  render() {
    window.scrollTo(0, 0);
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div style={{ margin: "1000px" }}>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div style={{ margin: "1000px" }}>Loading...</div>;
    } else {
      return (
        <div
          class="mx-auto"
          style={{ background: "#24282c", height: "4000px", width: "1500px" }}
        >
          <div class="col-md-8 blog-main" style={{ margin: "20px" }}>
            {items.map(NewsCase => (
              <div class="blog-post">
                <h2 class="blog-post-title" style={{ color: "white" }}>
                  {NewsCase.title}
                </h2>
                <p class="blog-post-meta" style={{ color: "white" }}>
                  {NewsCase.date}
                </p>

                <p style={{ color: "white" }}>{NewsCase.titleText}</p>
                <img src={NewsCase.picture} height="300px" />
                <hr style={{ bordercolor: "white" }} />
                <p style={{ color: "white" }}>{NewsCase.text}</p>
                <p style={{ color: "white" }}>{NewsCase.content}</p>
              </div>
            ))}
          </div>
        </div>
      );
    }
  }
}

const root = document.getElementById("root");
if (root)
  ReactDOM.render(
    <HashRouter>
      <div style={{ background: "#24282c" }}>
        <Menu />
        <LatestCases />
        <Route exact path="/" component={NewsList} />
        <Route path="/nysak" component={NewCase} />
        <Route path="/nysak" component={DeleteAndEdit} />
        <Route exact path="/nyheter" component={OnlyNewsList} />
        <Route exact path="/sport" component={SportList} />
        <Route exact path="/kultur" component={CultureList} />
        <Route exact path="/dataspill" component={GamesList} />
        <Route exact path="/politikk" component={PoliticsList} />
        <Route exact path="/:id" component={CaseDetails} />
        <Route exact path="/nyheter/:id" component={CaseDetails} />
        <Route exact path="/sport/:id" component={CaseDetails} />
        <Route exact path="/kultur/:id" component={CaseDetails} />
        <Route exact path="/dataspill/:id" component={CaseDetails} />
        <Route exact path="/politikk/:id" component={CaseDetails} />
        <Route exact path="/edit/:id" component={CaseEdit} />
      </div>
    </HashRouter>,
    root
  );
